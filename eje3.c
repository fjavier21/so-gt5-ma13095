#include <fcntl.h>
#include <unistd.h>

int main()
{
  char *from = "./source.txt";
  char buf[4096];
  int fd_from = open(from, O_RDONLY);
  ssize_t nread;

  if (fd_from < 0)
  {
    return -1;
  }

  while (nread = read(fd_from, buf, sizeof buf), nread > 0)
  {
    write(1, buf, nread);
  }
}

